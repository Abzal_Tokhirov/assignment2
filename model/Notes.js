const mongoose = require("mongoose");

const notesSchema = new mongoose.Schema({
  userId: {
    type: String,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model("Notes", notesSchema);
