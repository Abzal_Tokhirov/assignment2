const router = require("express").Router();
const verify = require("./verifyToken");
const User = require("../model/User");
const bcrypt = require("bcrypt");

router.get("/me", verify, async (req, res) => {
  const user = await User.findById({
    _id: req.user._id,
  });

  try {
    res.status(200).json({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },
    });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.delete("/me", verify, async (req, res) => {
  const user = await User.findById({
    _id: req.user._id,
  });
  try {
    await User.findByIdAndDelete({ _id: user._id });
    res.status(200).json({ message: "Success" });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.patch("/me", verify, async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const user = await User.findById({ _id: req.user._id });
  const salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(newPassword, salt);
  const validPassword = await bcrypt.compare(oldPassword, user.password);
  if (!validPassword) res.status(400).json({ message: "string" });

  try {
    await User.updateOne(user, { password: hashPassword });
    res.status(200).json({ message: "Success" });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: "string" });
  }
});

module.exports = router;
