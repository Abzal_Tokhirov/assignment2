const router = require("express").Router();
const User = require("../model/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
// Validation
// const Joi = require("@hapi/joi");

// const schema = Joi.object({
//   name: Joi.string().required().min(6),
//   password: Joi.string().required(),
// });

router.post("/register", async (req, res) => {
  const { username, password } = req.body;
  // Validation
  // const { error } = schema.validate(req.body);
  // res.json({ message: error.details[0].message.toString() });
  // if (error) res.status(400).json({ message: "failed validation" });

  // Checking if username exists
  const nameExists = await User.findOne({ username });
  if (nameExists) {
    return res.status(400).json({ message: "string" });
  }

  // Hash the password
  const salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(password, salt);
  // Create a new user
  const user = new User({
    username: username,
    password: hashPassword,
  });
  try {
    await user.save();
    res.status(200).json({ message: "Success" });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;
  // Checking if name exists
  const user = await User.findOne({ username });
  if (!user) {
    return res.status(400).json({ message: "string" });
  }
  // Password is correct
  const validPassword = await bcrypt.compare(password, user.password);
  if (!validPassword) return res.status(400).json({ message: "string" });

  // JWT
  const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
  try {
    res
      .status(200)
      .header("authorization", token)
      .json({ message: "Success", jwt_token: token });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});
module.exports = router;
