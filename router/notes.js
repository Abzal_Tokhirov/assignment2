const router = require("express").Router();
const verify = require("./verifyToken");
const Notes = require("../model/Notes");
router.get("/", verify, async (req, res) => {
  try {
    const notes = await Notes.find({ userId: req.user._id });

    const { offset, limit } = req.query;

    let offseted = notes.slice(offset);
    let limitedOffseted = offseted.slice(0, limit);

    res.status(200).json({
      offset: parseInt(offset) || 0,
      limit: parseInt(limit) || 0,
      count: notes.length,
      notes: limitedOffseted,
    });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.get("/:id", verify, async (req, res) => {
  const { id } = req.params;
  try {
    const noteById = await Notes.findById({ _id: id });
    if (!noteById) {
      return res.status(400).json({ message: "string" });
    }
    res.status(200).json({ noteById });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.post("/", verify, async (req, res) => {
  const { text } = req.body;
  const notes = new Notes({
    userId: req.user._id,
    text,
  });

  try {
    await notes.save();
    res.status(200).json({ message: "Success" });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.put("/:id", verify, async (req, res) => {
  const { id } = req.params;
  const { text: updatedText } = req.body;
  try {
    await Notes.findByIdAndUpdate({ _id: id }, { text: updatedText });
    const updatedNote = await Notes.findById({ _id: id });
    if (!updatedNote) {
      res.status(400).json({ message: "string" });
    } else {
      res.status(200).json({ message: "Success" });
    }
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.patch("/:id", verify, async (req, res) => {
  const { id } = req.params;
  try {
    const searchedNote = await Notes.findById({ _id: id });
    await Notes.updateOne(searchedNote, {
      completed: !searchedNote.completed,
    });

    res.status(200).json({ message: "Success" });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.delete("/:id", verify, async (req, res) => {
  const { id } = req.params;
  try {
    const deletedNote = await Notes.findByIdAndDelete({ _id: id });
    if (deletedNote) {
      Notes.deleteOne(deletedNote);
      res.status(200).json({ message: "Success" });
    } else {
      res.status(400).json({ message: "string" });
    }
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

module.exports = router;
