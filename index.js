const express = require("express");
const app = express();
const dotenv = require("dotenv");
const morgan = require("morgan");
const cors = require("cors");
const mongoose = require("mongoose");
const PORT = process.env.PORT || 8080;
morgan("tiny");
dotenv.config();
app.use(cors());
app.use(express.json());

mongoose.connect(process.env.DB_URL, () => {
  console.log("connected to database");
});
// Import routes
const authRoute = require("./router/auth");
const notesRoute = require("./router/notes");
const usersRoute = require("./router/users");
// Middlewares
app.use("/api/auth", authRoute);
app.use("/api/notes", notesRoute);
app.use("/api/users", usersRoute);

app.listen(PORT, () => {
  console.log("Server is running");
});
